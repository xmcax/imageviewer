package pl.adamnowicki.demo.imageviewer.infrastructure.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.adamnowicki.demo.imageviewer.image.IncomingFile;
import pl.adamnowicki.demo.imageviewer.image.Image;
import pl.adamnowicki.demo.imageviewer.image.ImageService;
import pl.adamnowicki.demo.imageviewer.image.ResultSet;
import pl.adamnowicki.demo.imageviewer.infrastructure.spring.JacksonConfiguration;

import java.time.LocalDateTime;
import java.util.List;

import static java.time.format.DateTimeFormatter.ofPattern;
import static java.util.Collections.singletonList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@WebMvcTest({ImageController.class, JacksonConfiguration.class})
public class ImageControllerTest {

    private static final String ID = "123cde";
    private static final String IMAGE_PATH = "/img/123.jpg";
    private static final String ORIGINAL_NAME = "originalName.jpg";
    private static final int FILE_SIZE = 1024;
    private static final LocalDateTime UPLOAD_DATE = LocalDateTime.of(2017, 4, 4, 10, 20);

    @Value("${app.resource.images.api}")
    String webLocation;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ImageService imageService;

    @Test
    public void getAllImages_shouldReturnListOfImages() throws Exception {
        Mockito.when(imageService.getAll(anyInt()))
                .thenReturn(domainModel());

        String uploadDate = UPLOAD_DATE.format(ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
        String imageUrl = webLocation + "/" + ORIGINAL_NAME;

        mockMvc.perform(get("/api/image?page=0"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.pageCount").value(1))
                .andExpect(jsonPath("$.images", hasSize(1)))
                .andExpect(jsonPath("$.images[0].id").value(ID))
                .andExpect(jsonPath("$.images[0].imageUrl").value(imageUrl))
                .andExpect(jsonPath("$.images[0].name").value(ORIGINAL_NAME))
                .andExpect(jsonPath("$.images[0].uploadedDate").value(uploadDate))
                .andExpect(jsonPath("$.images[0].size").value(FILE_SIZE));
    }

    @Test
    public void uploadNewImage_shouldPassTheFileToService() throws Exception {
        MockMultipartFile image = new MockMultipartFile("file", ORIGINAL_NAME, "image/jpeg", "sample image".getBytes());

        mockMvc.perform(fileUpload("/api/image")
                .file(image))
                .andExpect(status().isOk());

        ArgumentCaptor<IncomingFile> fileArgumentCaptor = ArgumentCaptor.forClass(IncomingFile.class);
        verify(imageService).uploadNewImage(fileArgumentCaptor.capture());

        assertThat(fileArgumentCaptor.getValue().getFileName()).isEqualTo(ORIGINAL_NAME);
    }

    @Test
    public void removeImage_shouldCallService() throws Exception {
        mockMvc.perform(delete("/api/image/" + ID))
            .andExpect(status().isOk());

        verify(imageService).removeImage(eq(ID));
    }

    static class MockResultSet<Image> implements ResultSet<Image> {
        private List<Image> items;

        MockResultSet(List<Image> items) {
            this.items = items;
        }

        @Override
        public List<Image> getItems() {
            return items;
        }

        @Override
        public long getTotalPageCount() {
            return items.size();
        }
    }

    private ResultSet<Image> domainModel() {
        return new MockResultSet<>(singletonList(
                Image.ImageBuilder.anImage()
                        .withImagePath(IMAGE_PATH)
                        .withName(ORIGINAL_NAME)
                        .withSize(FILE_SIZE)
                        .withHash(ID)
                        .withUploadDate(UPLOAD_DATE)
                        .build()
        ));
    }
}