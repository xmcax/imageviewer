package pl.adamnowicki.demo.imageviewer.infrastructure.db;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import pl.adamnowicki.demo.imageviewer.image.Image;
import pl.adamnowicki.demo.imageviewer.image.ImageNotFoundException;
import pl.adamnowicki.demo.imageviewer.image.ResultSet;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

public class ImageStoreAdapterTest {

    private static final long DB_ID = 123;
    private static final String ACTUAL_NAME_IN_STORAGE = "actualNameInStorage";
    private static final String UPLOADED_FILE_NAME = "uploadedFileName";
    private static final LocalDateTime UPLOAD_TIME = LocalDateTime.of(2017, 1, 2, 10, 0);
    private static final long FILE_SIZE = 1024;
    private static final String FILE_HASH = "123dfecd324";
    public static final int PAGE_SIZE = 20;

    private ImageStoreAdapter imageStoreAdapter;
    private ImageRepository imageRepository;

    @Before
    public void init() {
        imageRepository = Mockito.mock(ImageRepository.class);
        imageStoreAdapter = new ImageStoreAdapter(imageRepository, PAGE_SIZE);
    }

    @Test
    public void getAllImages_shouldMapDbModelIntoDomainModel() {
        Mockito.when(imageRepository.findAll(any(PageRequest.class)))
                .thenReturn(dbPageResult());

        ResultSet<Image> images = imageStoreAdapter.getImages(1);

        assertThat(images.getItems()).hasSize(1);
        Image image = images.getItems().get(0);
        assertThat(image.getId()).isEqualTo(FILE_HASH);
        assertThat(image.getImagePath()).isEqualTo(ACTUAL_NAME_IN_STORAGE);
        assertThat(image.getName()).isEqualTo(UPLOADED_FILE_NAME);
        assertThat(image.getSize()).isEqualTo(FILE_SIZE);
        assertThat(image.getUploadDate()).isEqualTo(UPLOAD_TIME);
    }

    @Test
    public void givenEntityExists_getSingleImage_shouldMapDbModelIntoDomainModel() {
        Mockito.when(imageRepository.findByHash(any()))
            .thenReturn(Optional.of(dbModel()));

        Image image = imageStoreAdapter.getImage(FILE_HASH);

        assertThat(image.getId()).isEqualTo(FILE_HASH);
        assertThat(image.getImagePath()).isEqualTo(ACTUAL_NAME_IN_STORAGE);
        assertThat(image.getName()).isEqualTo(UPLOADED_FILE_NAME);
        assertThat(image.getSize()).isEqualTo(FILE_SIZE);
        assertThat(image.getUploadDate()).isEqualTo(UPLOAD_TIME);
    }

    @Test(expected = ImageNotFoundException.class)
    public void givenEntityNotExists_getSingleImage_shouldThrowException() {
        Mockito.when(imageRepository.findByHash(any()))
            .thenReturn(Optional.empty());

        imageStoreAdapter.getImage(FILE_HASH);
    }

    @Test
    public void givenEntityExists_removeImage_shouldRemoveViaRepository() {
        Image image = domainModel();

        imageStoreAdapter.deleteImage(image);

        verify(imageRepository).deleteByHash(image.getHash());
    }

    private Page<ImageEntity> dbPageResult() {
        return new PageImpl<>(singletonList(dbModel()), null, 1);
    }

    private ImageEntity dbModel() {
        return new ImageEntity()
            .id(DB_ID)
            .filesystemName(ACTUAL_NAME_IN_STORAGE)
            .originalName(UPLOADED_FILE_NAME)
            .uploadTime(UPLOAD_TIME)
            .size(FILE_SIZE)
            .hash(FILE_HASH);
    }

    private Image domainModel() {
        return Image.ImageBuilder.anImage()
            .withId(FILE_HASH)
            .withImagePath(ACTUAL_NAME_IN_STORAGE)
            .withName(UPLOADED_FILE_NAME)
            .withUploadDate(UPLOAD_TIME)
            .withSize(FILE_SIZE)
            .withHash(FILE_HASH)
            .build();
    }
}