package pl.adamnowicki.demo.imageviewer.image;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import pl.adamnowicki.demo.imageviewer.hash.HashCalculator;
import pl.adamnowicki.demo.imageviewer.infrastructure.db.ImageStoreAdapter;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.adamnowicki.demo.imageviewer.image.Image.ImageBuilder.anImage;

public class ImageServiceTest {

    private static final String FILENAME = "filename.jpg";
    private static final String CONTENT = "imageBody";
    private static final String IMAGE_ID = "123cde";

    private ImageService imageService;
    private ImageStoreAdapter imageStoreAdapter;
    private FileLocationBuilder fileLocationBuilder;
    private Clock fixedClock;
    private HashCalculator hashCalculator;
    private File tmpFolder;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Before
    public void init() throws IOException {
        imageStoreAdapter = Mockito.mock(ImageStoreAdapter.class);
        hashCalculator = Mockito.mock(HashCalculator.class);
        fileLocationBuilder = Mockito.mock(FileLocationBuilder.class);
        fixedClock = Clock.fixed(Instant.now(), ZoneId.systemDefault());
        tmpFolder = folder.getRoot();

        imageService = new ImageService(imageStoreAdapter, fixedClock, fileLocationBuilder, hashCalculator);
    }

    @Test
    public void getImages_shouldReturnImaged() {
        ResultSet resultSet = new MockResultSet();
        when(imageStoreAdapter.getImages(anyInt()))
                .thenReturn(resultSet);

        ResultSet<Image> all = imageService.getAll(1);

        assertThat(all).isEqualTo(resultSet);
    }

    @Test
    public void uploadImage_shouldStoreEntity() {
        Path path = Paths.get(tmpFolder.toString(), FILENAME);
        when(fileLocationBuilder.getLocationForImage(any())).thenReturn(path);

        imageService.uploadNewImage(new MockIncomingFile(FILENAME, CONTENT));

        ArgumentCaptor<Image> imageArgumentCaptor = ArgumentCaptor.forClass(Image.class);
        verify(imageStoreAdapter).storeImage(imageArgumentCaptor.capture());

        Image capturedImage = imageArgumentCaptor.getValue();
        assertThat(capturedImage.getUploadDate()).isEqualByComparingTo(LocalDateTime.now(fixedClock));
        assertThat(capturedImage.getSize()).isEqualTo(CONTENT.getBytes().length);
        assertThat(capturedImage.getName()).isEqualTo(FILENAME);
        assertThat(capturedImage.getImagePath()).isEqualTo(path.toString());
    }

    @Test
    public void uploadImage_shouldStoreFileInFilesystem() {
        Path path = Paths.get(tmpFolder.toString(), FILENAME);
        when(fileLocationBuilder.getLocationForImage(any())).thenReturn(path);

        imageService.uploadNewImage(new MockIncomingFile(FILENAME, CONTENT));

        assertThat(path.toFile().exists());
    }

    @Test
    public void deleteImage_shouldRemoveEntity() throws IOException {
        File imageFile = folder.newFile(FILENAME);

        Image image = anImage()
            .withHash(IMAGE_ID)
            .withImagePath(imageFile.toString())
            .build();
        when(imageStoreAdapter.getImage(eq(IMAGE_ID)))
            .thenReturn(image);

        imageService.removeImage(IMAGE_ID);

        verify(imageStoreAdapter).deleteImage(image);
    }

    @Test
    public void deleteImage_shouldRemoveFileFromFilesystem() throws IOException {
        File imageFile = folder.newFile(FILENAME);

        Image image = anImage()
            .withHash(IMAGE_ID)
            .withImagePath(imageFile.toString())
            .build();
        when(imageStoreAdapter.getImage(eq(IMAGE_ID)))
            .thenReturn(image);

        imageService.removeImage(IMAGE_ID);

        assertThat(imageFile.exists()).isFalse();
    }

    class MockResultSet implements ResultSet<Image> {
        @Override
        public List<Image> getItems() {
            return null;
        }

        @Override
        public long getTotalPageCount() {
            return 0;
        }
    }

    class MockIncomingFile implements IncomingFile {
        private final String name;
        private final String content;

        MockIncomingFile(String name, String content) {
            this.name = name;
            this.content = content;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return new ByteArrayInputStream(content.getBytes());
        }

        @Override
        public String getFileName() {
            return name;
        }

        @Override
        public void transferTo(Path dest) throws IOException {
            try (FileOutputStream osw = new FileOutputStream(dest.toFile())) {
                osw.write(content.getBytes());
            }
        }
    }
}