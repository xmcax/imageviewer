package pl.adamnowicki.demo.imageviewer.infrastructure.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "image", indexes = {
    @Index(columnList = "id", name = "image_id_idx"),
    @Index(columnList = "hash", name = "image_hash_idx")
})
class ImageEntity {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Column(name = "original_name")
    String originalName;

    @Column(name = "size")
    Long size;

    @Column(name = "upload_time")
    LocalDateTime uploadTime;

    @Column(name = "fs_name")
    String filesystemName;

    @Column(name = "hash", unique = true)
    String hash;

    Long getId() {
        return id;
    }

    void setId(Long id) {
        this.id = id;
    }

    ImageEntity id(Long id) {
        setId(id);
        return this;
    }

    String getOriginalName() {
        return originalName;
    }

    void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    ImageEntity originalName(String originalName) {
        setOriginalName(originalName);
        return this;
    }

    Long getSize() {
        return size;
    }

    void setSize(Long size) {
        this.size = size;
    }

    ImageEntity size(Long size) {
        setSize(size);
        return this;
    }

    LocalDateTime getUploadTime() {
        return uploadTime;
    }

    void setUploadTime(LocalDateTime uploadTime) {
        this.uploadTime = uploadTime;
    }

    ImageEntity uploadTime(LocalDateTime uploadTime) {
        setUploadTime(uploadTime);
        return this;
    }

    String getFilesystemName() {
        return filesystemName;
    }

    void setFilesystemName(String filesystemName) {
        this.filesystemName = filesystemName;
    }

    ImageEntity filesystemName(String filesystemName) {
        setFilesystemName(filesystemName);
        return this;
    }

    String getHash() {
        return hash;
    }

    void setHash(String hash) {
        this.hash = hash;
    }

    ImageEntity hash(String hash) {
        setHash(hash);
        return this;
    }
}
