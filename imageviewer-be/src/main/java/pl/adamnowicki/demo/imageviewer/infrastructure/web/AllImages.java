package pl.adamnowicki.demo.imageviewer.infrastructure.web;

import java.util.List;

public class AllImages {
    private List<ImageDetails> images;
    private long pageCount;

    AllImages() {
    }

    AllImages(List<ImageDetails> images, long pageCount) {
        this.images = images;
        this.pageCount = pageCount;
    }

    public List<ImageDetails> getImages() {
        return images;
    }

    public long getPageCount() {
        return pageCount;
    }
}
