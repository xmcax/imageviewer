package pl.adamnowicki.demo.imageviewer.image;

import java.nio.file.Path;
import java.nio.file.Paths;

public class FileLocationBuilder {
  private final String storagePath;

  public FileLocationBuilder(String storagePath) {
    this.storagePath = storagePath;
  }

  Path getLocationForImage(String filename) {
    return Paths.get(storagePath, filename);
  }
}
