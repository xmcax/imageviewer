package pl.adamnowicki.demo.imageviewer.hash;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashCalculator {

  private static final String HASH_ALOGRITHM = "SHA-1";

  public String calculate(File file) {
    byte[] byteArray = new byte[1024];
    int bytesCount;

    try(InputStream is = new FileInputStream(file)) {
      MessageDigest digest = MessageDigest.getInstance(HASH_ALOGRITHM);

      while ((bytesCount = is.read(byteArray)) != -1) {
        digest.update(byteArray, 0, bytesCount);
      }
      byte[] bytes = digest.digest();

      StringBuilder sb = new StringBuilder();
      for (byte aByte : bytes) {
        sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
      }

      return sb.toString();
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException("Message Digest algorithm not recognized: " + HASH_ALOGRITHM);
    } catch (FileNotFoundException e) {
      throw new RuntimeException("Attempt to calculate hash failed, file not found: " + file.toString());
    } catch (IOException e) {
      throw new RuntimeException("I/O error during hash calculation");
    }
  }
}
