package pl.adamnowicki.demo.imageviewer.infrastructure.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.PathResourceResolver;

import java.net.URI;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter {

    @Value("${app.cache.ttl-days}")
    private long cacheTimeToLiveDays;

    @Value("${app.resource.images.filesystem}")
    private String imagesFilesystem;

    @Value("${app.resource.images.api}")
    private String imagesApi;

    @Value("${app.resource.webapp.filesystem}")
    private String webappFilesystem;

    @Value("${app.resource.webapp.api}")
    private String webappApi;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        long CACHE_TIME_TO_LIVE = TimeUnit.DAYS.toSeconds(cacheTimeToLiveDays);

        URI imagesFilesystemUri = Paths.get(imagesFilesystem).toUri();
        registry.addResourceHandler(imagesApi + "/**")
                .addResourceLocations(imagesFilesystemUri.toString())
                .setCachePeriod((int)CACHE_TIME_TO_LIVE)
                .resourceChain(true)
                .addResolver(new PathResourceResolver());

        URI webappFilesystemUri = Paths.get(webappFilesystem).toUri();
        registry.addResourceHandler(webappApi + "/**")
                .addResourceLocations(webappFilesystemUri.toString())
                .setCachePeriod((int)CACHE_TIME_TO_LIVE)
                .resourceChain(true)
                .addResolver(new PathResourceResolver());

        super.addResourceHandlers(registry);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/index.html");
    }
}
