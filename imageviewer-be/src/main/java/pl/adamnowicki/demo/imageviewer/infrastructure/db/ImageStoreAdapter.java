package pl.adamnowicki.demo.imageviewer.infrastructure.db;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import pl.adamnowicki.demo.imageviewer.image.Image;
import pl.adamnowicki.demo.imageviewer.image.ImageNotFoundException;
import pl.adamnowicki.demo.imageviewer.image.ResultSet;

import javax.persistence.EntityNotFoundException;
import java.util.function.Function;

@Service
public class ImageStoreAdapter {

    private final ImageRepository imageRepository;
    private final int pageSize;

    private Function<ImageEntity, Image> intoDomain = entity -> new Image.ImageBuilder()
            .withId(entity.getHash())
            .withName(entity.getOriginalName())
            .withSize(entity.getSize())
            .withUploadDate(entity.getUploadTime())
            .withImagePath(entity.getFilesystemName())
            .withHash(entity.getHash())
            .build();

    private Function<Image, ImageEntity> fromDomain = model -> new ImageEntity()
            .originalName(model.getName())
            .filesystemName(model.getImagePath())
            .size(model.getSize())
            .hash(model.getHash())
            .uploadTime(model.getUploadDate());

    ImageStoreAdapter(ImageRepository imageRepository, @Value("${app.page-size}") int pageSize) {
        this.imageRepository = imageRepository;
        this.pageSize = pageSize;
    }

    public ResultSet<Image> getImages(int page) {
        Page<ImageEntity> pageResult = imageRepository.findAll(new PageRequest(page, pageSize));
        return new DbResultSet<>(pageResult, intoDomain, pageResult.getTotalPages());
    }

    public Image getImage(String hash) {
        ImageEntity imageEntity = imageRepository.findByHash(hash)
            .orElseThrow(() -> new ImageNotFoundException("Image not found: " + hash));
        return intoDomain.apply(imageEntity);
    }

    public Image storeImage(Image image) {
      ImageEntity savedEntity = imageRepository.save(fromDomain.apply(image));
      return intoDomain.apply(savedEntity);
    }

    public void deleteImage(Image image) {
        imageRepository.deleteByHash(image.getId());
    }
}
