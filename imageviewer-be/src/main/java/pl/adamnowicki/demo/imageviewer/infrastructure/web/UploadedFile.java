package pl.adamnowicki.demo.imageviewer.infrastructure.web;

import org.springframework.web.multipart.MultipartFile;
import pl.adamnowicki.demo.imageviewer.image.IncomingFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

public class UploadedFile implements IncomingFile {
    private final MultipartFile file;
    UploadedFile(MultipartFile file) {
        this.file = file;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return file.getInputStream();
    }

    @Override
    public String getFileName() {
        return file.getOriginalFilename();
    }

    @Override
    public void transferTo(Path dest) throws IOException {
        file.transferTo(dest.toFile());
    }
}
