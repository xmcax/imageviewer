package pl.adamnowicki.demo.imageviewer.infrastructure.web;

import java.time.LocalDateTime;

class ImageDetails {
    private final String id;
    private final String name;
    private final LocalDateTime uploadedDate;
    private final String imageUrl;
    private final Long size;

    private ImageDetails(String id, String name, LocalDateTime uploadedDate, String imageUrl, Long size) {
        this.id = id;
        this.name = name;
        this.uploadedDate = uploadedDate;
        this.imageUrl = imageUrl;
        this.size = size;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getUploadedDate() {
        return uploadedDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Long getSize() {
        return size;
    }

    static final class ImageDetailsBuilder {
        private String id;
        private String name;
        private LocalDateTime uploadedDate;
        private String imageUrl;
        private Long size;

        ImageDetailsBuilder() {
        }

        static ImageDetailsBuilder anImageDetails() {
            return new ImageDetailsBuilder();
        }

        ImageDetailsBuilder withId(String id) {
            this.id = id;
            return this;
        }

        ImageDetailsBuilder withName(String name) {
            this.name = name;
            return this;
        }

        ImageDetailsBuilder withUploadedDate(LocalDateTime uploadedDate) {
            this.uploadedDate = uploadedDate;
            return this;
        }

        ImageDetailsBuilder withImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
            return this;
        }

        ImageDetailsBuilder withSize(Long size) {
            this.size = size;
            return this;
        }

        ImageDetails build() {
            ImageDetails imageDetails = new ImageDetails(id, name, uploadedDate, imageUrl, size);
            return imageDetails;
        }
    }
}
