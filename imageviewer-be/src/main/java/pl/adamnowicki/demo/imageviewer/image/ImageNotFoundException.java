package pl.adamnowicki.demo.imageviewer.image;

public class ImageNotFoundException extends RuntimeException {
    public ImageNotFoundException(String s) {
        super(s);
    }
}
