package pl.adamnowicki.demo.imageviewer.image;

import java.util.List;

public interface ResultSet<T> {
    List<T> getItems();
    long getTotalPageCount();
}
