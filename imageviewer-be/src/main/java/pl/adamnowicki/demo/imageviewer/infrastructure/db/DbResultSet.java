package pl.adamnowicki.demo.imageviewer.infrastructure.db;

import org.springframework.data.domain.Page;
import pl.adamnowicki.demo.imageviewer.image.ResultSet;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DbResultSet<R, T> implements ResultSet {
    Page<R> page;
    Function<R, T> resultTransformer;
    int pageCount;

    public DbResultSet(Page<R> page, Function<R,T> resultTransformer, int pageCount) {
        this.page = page;
        this.resultTransformer = resultTransformer;
        this.pageCount = pageCount;
    }

    @Override
    public List<T> getItems() {
        return page.getContent()
                .stream()
                .map(resultTransformer)
                .collect(Collectors.toList());
    }

    @Override
    public long getTotalPageCount() {
        return pageCount;
    }
}
