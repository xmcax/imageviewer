package pl.adamnowicki.demo.imageviewer.image;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

public interface IncomingFile {
    InputStream getInputStream() throws IOException;
    String getFileName();
    void transferTo(Path dest) throws IOException;
}
