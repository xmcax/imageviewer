package pl.adamnowicki.demo.imageviewer.infrastructure.db;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface ImageRepository extends PagingAndSortingRepository<ImageEntity,Long> {
  Optional<ImageEntity> findByHash(String hash);
  void deleteByHash(String hash);
}
