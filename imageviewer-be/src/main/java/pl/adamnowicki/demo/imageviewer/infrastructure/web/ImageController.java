package pl.adamnowicki.demo.imageviewer.infrastructure.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.adamnowicki.demo.imageviewer.image.Image;
import pl.adamnowicki.demo.imageviewer.image.ImageNotFoundException;
import pl.adamnowicki.demo.imageviewer.image.ImageService;
import pl.adamnowicki.demo.imageviewer.image.ResultSet;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/image")
class ImageController {

    private final ImageService imageService;
    private final Function<Image, ImageDetails> intoView;

    @Autowired
    public ImageController(ImageService imageService,
                           @Value("${app.resource.images.api}") final String webLocation) {
        this.imageService = imageService;

        intoView = image -> new ImageDetails.ImageDetailsBuilder()
                .withId(image.getHash())
                .withName(image.getName())
                .withImageUrl(webLocation + "/" + image.getName())
                .withUploadedDate(image.getUploadDate())
                .withSize(image.getSize())
                .build();
    }

    @GetMapping
    AllImages getAllImages(@RequestParam("page") int page) {
        ResultSet<Image> all = imageService.getAll(page);

        List<ImageDetails> view = all.getItems().stream()
                .map(intoView)
                .collect(Collectors.toList());

        return new AllImages(view, all.getTotalPageCount());
    }

    @PostMapping
    void uploadNewImage(@RequestPart(name = "file") MultipartFile file) {
        UploadedFile uploadedFile = new UploadedFile(file);
        imageService.uploadNewImage(uploadedFile);
    }

    @DeleteMapping("{imageId}")
    ResponseEntity<?> deleteImage(@PathVariable("imageId") String imageId) {
        try {
            imageService.removeImage(imageId);
            return ResponseEntity.ok().build();
        } catch(ImageNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

}
