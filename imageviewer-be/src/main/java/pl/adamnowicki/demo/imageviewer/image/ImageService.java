package pl.adamnowicki.demo.imageviewer.image;

import org.springframework.transaction.annotation.Transactional;
import pl.adamnowicki.demo.imageviewer.hash.HashCalculator;
import pl.adamnowicki.demo.imageviewer.infrastructure.db.ImageStoreAdapter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Clock;
import java.time.LocalDateTime;

import static pl.adamnowicki.demo.imageviewer.image.Image.ImageBuilder.anImage;

@Transactional
public class ImageService {

    private final ImageStoreAdapter imageStoreAdapter;
    private final FileLocationBuilder fileLocationBuilder;
    private final Clock clock;
    private final HashCalculator hashCalculator;

    public ImageService(ImageStoreAdapter imageRepository, Clock clock, FileLocationBuilder fileLocationBuilder, HashCalculator hashCalculator) {
        this.imageStoreAdapter = imageRepository;
        this.fileLocationBuilder = fileLocationBuilder;
        this.clock = clock;
        this.hashCalculator = hashCalculator;
    }

    public ResultSet<Image> getAll(int page) {
        return imageStoreAdapter.getImages(page);
    }

    public Image uploadNewImage(IncomingFile incomingIncomingFile) {
        Path dstPath = storeFile(incomingIncomingFile);
        String hash = hashCalculator.calculate(dstPath.toFile());

        Image image = anImage()
                .withName(dstPath.getFileName().toString())
                .withSize(dstPath.toFile().length())
                .withHash(hash)
                .withUploadDate(LocalDateTime.now(clock))
                .withImagePath(dstPath.toString())
                .build();

        return imageStoreAdapter.storeImage(image);
    }

    private Path storeFile(IncomingFile incoming) {
        Path tmpPath = fileLocationBuilder.getLocationForImage(incoming.getFileName());

        try {
            incoming.transferTo(tmpPath);
        } catch (IOException e) {
            throw new RuntimeException("Attempt to store file failed: " + incoming.getFileName());
        }
        return tmpPath;
    }

    public void removeImage(String imageId) {
        Image image = imageStoreAdapter.getImage(imageId);
        Path path = Paths.get(image.getImagePath());
        try {
            Files.delete(path);
            imageStoreAdapter.deleteImage(image);
        } catch (IOException e) {
            throw new RuntimeException("Attempt to delete file failed: " + image.getImagePath());
        }

    }
}
