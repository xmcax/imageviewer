package pl.adamnowicki.demo.imageviewer.infrastructure.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.adamnowicki.demo.imageviewer.hash.HashCalculator;
import pl.adamnowicki.demo.imageviewer.image.FileLocationBuilder;
import pl.adamnowicki.demo.imageviewer.image.ImageService;
import pl.adamnowicki.demo.imageviewer.infrastructure.db.ImageStoreAdapter;

import java.time.Clock;

@Configuration
public class BeanProvider {

    @Value("${app.resource.images.filesystem}")
    private String storagePath;

    @Bean
    ImageService imageService(ImageStoreAdapter imageStoreAdapter, Clock clock,
                              FileLocationBuilder fileLocationBuilder,
                              HashCalculator hashCalculator) {
        return new ImageService(imageStoreAdapter, clock, fileLocationBuilder, hashCalculator);
    }

    @Bean
    HashCalculator hashCalculator() {
        return new HashCalculator();
    }

    @Bean
    FileLocationBuilder fileLocationBuilder() {
        return new FileLocationBuilder(storagePath);
    }

    @Bean
    Clock clock() {
        return Clock.systemDefaultZone();
    }
}
