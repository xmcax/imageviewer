package pl.adamnowicki.demo.imageviewer.image;

import java.time.LocalDateTime;

public class Image {
    private String id;
    private String name;
    private String hash;
    private long size;
    private LocalDateTime uploadDate;
    private String imagePath;

    Image(String id, String name, String hash, long size,
                 LocalDateTime uploadDate, String imagePath) {
        this.id = id;
        this.name = name;
        this.hash = hash;
        this.size = size;
        this.uploadDate = uploadDate;
        this.imagePath = imagePath;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getHash() {
        return hash;
    }

    public long getSize() {
        return size;
    }

    public LocalDateTime getUploadDate() {
        return uploadDate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public static final class ImageBuilder {
        private String id;
        private String name;
        private String hash;
        private long size;
        private LocalDateTime uploadDate;
        private String imagePath;

        public ImageBuilder() {
        }

        public static ImageBuilder anImage() {
            return new ImageBuilder();
        }

        public ImageBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public ImageBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ImageBuilder withHash(String hash) {
            this.hash = hash;
            return this;
        }

        public ImageBuilder withSize(long size) {
            this.size = size;
            return this;
        }

        public ImageBuilder withUploadDate(LocalDateTime uploadDate) {
            this.uploadDate = uploadDate;
            return this;
        }

        public ImageBuilder withImagePath(String imagePath) {
            this.imagePath = imagePath;
            return this;
        }

        public Image build() {
            return new Image(id, name, hash, size, uploadDate, imagePath);
        }
    }
}
