'use strict';

import gulp from 'gulp';
import webpack from 'webpack';
import path from 'path';
import sync from 'run-sequence';
import fs from 'fs';
import yargs from 'yargs';
import gutil from 'gulp-util';
import serve from 'browser-sync';
import express from 'express';
import webpackDevMiddelware from 'webpack-dev-middleware';
import webpachHotMiddelware from 'webpack-hot-middleware';
import proxyMiddleware from 'http-proxy-middleware';
import colorsSupported from 'supports-color';

let root = 'src';

// helper method for resolving paths
let resolveToApp = (glob = '') => {
    return path.join(root, 'app', glob); // app/{glob}
};

let resolveToComponents = (glob = '') => {
    return path.join(root, 'app/components', glob); // app/components/{glob}
};

// map of all paths
let paths = {
    js: resolveToComponents('**/*!(.spec.js).js'), // exclude spec files
    styl: resolveToApp('**/*.styl'), // stylesheets
    html: [
        resolveToApp('**/*.html'),
        path.join(root, 'index.html')
    ],
    entry: path.join(__dirname, root, 'app/app.js'),
    output: root,
    blankTemplates: path.join(__dirname, 'generator', 'component/**/*.**')
};

// use webpack.config.js to build modules
gulp.task('webpack', (cb) => {
    const config = require('./webpack.dist.config');
    config.entry.app = paths.entry;

    webpack(config, (err, stats) => {
        if (err) {
            throw new gutil.PluginError("webpack", err);
        }

        gutil.log("[webpack]", stats.toString({
            colors: colorsSupported,
            chunks: false,
            errorDetails: true
        }));

        cb();
    });
});

gulp.task('serve', () => {
    const config = require('./webpack.dev.config');

    config.entry.app = [
        // this modules required to make HRM working
        // it responsible for all this webpack magic
        'webpack-hot-middleware/client?reload=true',
        // application entry point
        paths.entry
    ];

    var devConfig = config.devServer;
    var app = express();
    var compiler = webpack(config);

    // app.use(express.static(devConfig.contentBase || __dirname));
    app.use(webpackDevMiddelware(compiler, {
        stats: {
            colors: colorsSupported,
            chunks: false,
            modules: false
        },
        publicPath: config.output.publicPath
    }));
    app.use(webpachHotMiddelware(compiler));

    // Set up the proxy.
    if (devConfig.proxy) {
        Object.keys(devConfig.proxy).forEach(function(context) {
            app.use(proxyMiddleware(context, devConfig.proxy[context]));
        });
    }

    if (devConfig.historyApiFallback) {
        console.log('404 responses will be forwarded to /index.html');

        app.get('*', function(req, res) {
            res.sendFile(path.resolve(devConfig.contentBase, 'index.html'));
        });
    }

    app.listen(devConfig.port || 8081, function() {
        console.log('Development server listening on port ' + devConfig.port);
    });

});

gulp.task('watch', ['serve']);

gulp.task('default', ['serve']);