## Preconditions
Make sure you have yarn installed

## Project setup
yarn install

## Running dev 
yarn start

## Running tests
yarn test

## Building application
yarn build

## Remarks
project structure based on: NG6-starter TodoMVC Example
