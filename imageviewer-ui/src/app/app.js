import angular from 'angular';

import 'normalize.css';
import './style/global.less';

import components from './components';
import services from './services';

import { appComponent } from './app.component';

angular.module('app', [
        services.name,
        components.name
    ])
    .component('app', appComponent);

angular.bootstrap(document.body, ['app'])