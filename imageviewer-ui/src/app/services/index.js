import angular from 'angular';

import 'angular-resource';

import { Image } from './image.service';

export default angular
    .module('app.services', [
        'ngResource'
    ])
    .service('Image', Image);