Image.$inject = ['$resource'];

export function Image($resource) {
    var resourceUrl = 'api/image/:id';

    return $resource(resourceUrl, {}, {
        'getAll': {
            method: 'GET',
            isArray: false
        },
        'delete': {
            method: 'DELETE'
        }
    });
}