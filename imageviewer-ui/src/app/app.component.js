import './app.less';

AppController.$inject = ['Image', '$rootScope', '$location'];

function AppController(Image, $rootScope, $location) {
    const refresh = () => Image.getAll({ page: this.currentPage }).$promise
        .then(imageResponse => {
            this.pageCount = imageResponse.pageCount;
            this.images = imageResponse.images;
        });

    const remove = id => Image.delete({ id }).$promise
        .then(refresh);

    angular.extend(this, {
        refresh,
        remove
    })

    this.$onInit = () => {
        const { page } = $location.search();
        this.currentPage = page || 0;

        const isNewPage = (page) => parseInt(page) === page && page !== this.currentPage;

        this.unregister = $rootScope.$on('$locationChangeSuccess', event => {
            const { page } = $location.search();
            if (isNewPage(page)) {
                this.currentPage = page
                refresh();
            }
        })

        refresh();
    };

    this.$onDestroy = () => {
        this.unregister();
    }
}

export var appComponent = {
    template: require('./app.component.html'),
    controller: AppController,
    controllerAs: 'vm'
};