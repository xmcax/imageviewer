import './toolbar.less';

ToolbarController.$inject = ['Upload', '$location'];

function ToolbarController(Upload, $location) {
    const clearUploadWidget = () => this.file = null;

    const upload = file => {
        Upload.upload({
                url: 'api/image',
                file
            }).then(clearUploadWidget)
            .then(this.onUpload);
    };

    const changePage = page => {
        $location.search('page', page);
    }

    angular.extend(this, {
        upload,
        changePage
    });

    this.$onChanges = function({ pageCount }) {
        if (pageCount && pageCount.currentValue) {
            this.pages = [...Array(pageCount.currentValue || 1).keys()];
        }
    }
}

export var toolbar = {
    template: require('./toolbar.html'),
    controller: ToolbarController,
    controllerAs: 'vm',
    bindings: {
        currentPage: '<',
        pageCount: '<',
        onUpload: '&'
    }
};