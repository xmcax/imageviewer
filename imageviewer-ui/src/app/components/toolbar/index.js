import angular from 'angular';

import 'ng-file-upload';

import { toolbar } from './toolbar.component';

export default angular
    .module('app.toolbar', [
        'ngFileUpload'
    ])
    .component('toolbar', toolbar);