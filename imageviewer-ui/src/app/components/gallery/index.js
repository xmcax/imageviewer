import angular from 'angular';

import { gallery } from './gallery.component';

export default angular
    .module('app.gallery', [])
    .component('gallery', gallery);