import './gallery.less';

function GalleryController() {
    let remove = id => {
        this.onRemove({ id });
    }

    angular.extend(this, {
        remove
    });
}

export var gallery = {
    template: require('./gallery.html'),
    controller: GalleryController,
    controllerAs: 'vm',
    bindings: {
        images: '<',
        onRemove: '&'
    }
};