import angular from 'angular';

import gallery from './gallery';
import toolbar from './toolbar';

export default angular
    .module('app.components', [
        toolbar.name,
        gallery.name
    ]);