var webpack = require('webpack');
var path = require('path');
var config = require('./webpack.config');

config.output = {
    filename: '[name].bundle.js',
    publicPath: '/',
    path: path.resolve(__dirname, 'client')
};

config.plugins = config.plugins.concat([
    // Adds webpack HMR support. It act's like livereload,
    // reloading page after webpack rebuilt modules.
    // It also updates stylesheets and inline assets without page reloading.
    new webpack.HotModuleReplacementPlugin()
]);

config.devServer = {
    port: 8081,
    contentBase: './src',
    historyApiFallback: true,
    proxy: {
        '/api': {
            target: 'http://127.0.0.1:8080/',
        },
        '/img': {
            target: 'http://127.0.0.1:8080/',
        }
    }
};

module.exports = config;