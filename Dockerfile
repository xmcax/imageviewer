FROM openjdk:8-jre-alpine

COPY imageviewer-be/build/libs/imageviewer-be-0.0.1-SNAPSHOT.jar  /opt/app/app.jar
COPY imageviewer-ui/dist /var/www

ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "/opt/app/app.jar"]

EXPOSE 8080
