### Setup complete app via docker

1.  Create docker volumes
	
	docker volume create --name=imageviewer-db
	docker volume create --name=img-storage

2.  Build image

	./gradlew createDockerImage

3.  Start application

	docker-compose -f app.yml up

### Development

For development purposes it's possible to start application without Docker:

1.  Run frontend with hot-reload and proxy pass to server 
 
	cd imageviewer-ui
	yarn start

2.  Create image (if not yet created) & run db
   
	docker volume create --name=imageviewer-db
	docker-compose -f postgresql.yml up -d

3.  Adjust application.yml according to your OS & Run backend 

	cd imageviewer-be
	./gradlew bootRun
 
### Notes

1. psql -h localhost -p 5432 -U user mydb
